# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=glm
pkgver=0.9.9.7
pkgrel=0
pkgdesc="C++ mathematics library for graphics programming"
url="https://glm.g-truc.net/"
arch="noarch"
license="MIT"
makedepends="cmake"
subpackages="$pkgname-dev $pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/g-truc/glm/archive/$pkgver.tar.gz
	fix-endian-test.patch
	glm.pc
	"
patch_args="--binary -p1"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DGLM_TEST_ENABLE=TRUE
	make -C build
}

check() {
	make -C build test
}

package() {
	mkdir -p "$pkgdir"/usr/include/
	cp -r glm "$pkgdir"/usr/include/
	mkdir -p "$pkgdir"/usr/share/doc
	cp -r doc "$pkgdir"/usr/share/doc/glm
	mkdir -p "$pkgdir"/usr/lib/pkgconfig
	cp "$srcdir"/glm.pc "$pkgdir"/usr/lib/pkgconfig/
}

sha512sums="9c557788d6382777317c94f8b30bc3df7e533877705514fa5d384f97b076d6bc750e841acbecdec8113e21af07bd8850159f5f1e079aaa2cde25540b480f983b  glm-0.9.9.7.tar.gz
93e02285b7530bb5fdad71905a55150e64cda3d036f43fca166eddb2e8d2f2d03025543c96dfd44234a37f860ea0682be2c683a66c9d4ef33f5bc269c95bbdfa  fix-endian-test.patch
185a9eae06b4bd291c72351239a969e37b83feb1b2de64c397f657370aff81241bf489f0109c74d50cd7106389c2740b0f620f39cdd3604dc51ed9b5046442af  glm.pc"
